## About

A binding to the [KiWi] library.  Adheres fairly closely to the
original API with renamed identifiers where it makes sense.
Additionally to that there's a high-level SXML interface for creating
widgets in a more declarative manner.

## Docs

See [its wiki page].

[KiWi]: https://github.com/mobius3/KiWi
[its wiki page]: http://wiki.call-cc.org/eggref/5/kiwi
