(import scheme)
(cond-expand
 (chicken-4
  (use (prefix sdl2 sdl2:)
       (prefix kiwi kw:))
  (import (only sdl2-internals unwrap-renderer unwrap-window)))
 (chicken-5
  (import (chicken base))
  (import (prefix sdl2 sdl2:))
  (import (prefix kiwi kw:))
  (import (only sdl2-internals unwrap-renderer unwrap-window))))

(sdl2:set-main-ready!)
(sdl2:init! '(everything))

(define width 320)
(define height 240)

(define-values (window renderer)
  (sdl2:create-window-and-renderer! width height))

(sdl2:render-draw-color-set! renderer (sdl2:make-color 10 10 10))

(define driver (kw:create-sdl2-driver (unwrap-renderer renderer)
                                      (unwrap-window window)))

(define tileset (kw:load-surface driver "tileset.png"))
(define gui (kw:init! driver tileset))

(define font (kw:load-font driver "Fontin-Regular.ttf" 12))
(kw:gui-font-set! gui font)

(kw:widgets gui
 `(frame (@ (x 0) (y 0) (w ,width) (h ,height))
   (scrollbox
    (@ (x 10) (y 10) (w ,(- width 20)) (h ,(- height 20)))
    (editbox (@ (id input) (x 20) (y 20) (w 180) (h 30) (text "User input")))
    (button (@ (x 20) (y 50) (w 80) (h 30) (text "OK")
               (mouse-down ,(lambda _ (print "Text: "
                                             (kw:editbox-text
                                              (kw:widget-by-id 'input)))))))
    (button (@ (x 120) (y 50) (w 80) (h 30) (text "Cancel")
               (mouse-down ,(lambda _ (print ":(")))))
    (checkbox (@ (x 20) (y 90) (w 100) (h 30) (text "TODO")))
    (toggle (@ (x 120) (y 90) (w 80) (h 30)
               (mouse-down ,(lambda _ (print "toggled!")))))
    (frame
     (@ (x 20) (y 130) (w 180) (h 50))
     (label
      (@ (x 10) (y 10) (w 160) (h 30)
         (icon ((x 0) (y 48) (w 24) (h 24)))
         (text "Ceci n'est pas un kiwi")))))))

(let loop ()
  (when (not (sdl2:quit-requested?))
    (sdl2:render-clear! renderer)
    (kw:process-events! gui)
    (kw:paint! gui)
    (sdl2:delay! 1)
    (sdl2:render-present! renderer)
    (loop)))

(kw:quit! gui)
(sdl2:quit!)
