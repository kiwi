(module kiwi
  (create-sdl2-driver release-driver!
   driver? driver-sdl2-renderer driver-sdl2-window
   surface? load-surface release-surface!
   font? load-font release-font!
   gui? init! process-events! paint! quit!
   gui-driver gui-driver-set!
   gui-tileset-surface gui-tileset-surface-set!
   gui-font gui-font-set!
   gui-text-color gui-text-color-set!
   rect rect? rect-x rect-y rect-w rect-h rect-x-set! rect-y-set! rect-w-set! rect-h-set!
   rect-empty? enclosing-rect rect-center-in-parent! rect-center-in-parent-vertically! rect-center-in-parent-horizontally! rect-layout-vertically! rect-layout-horizontally! rect-fill-parent-vertically! rect-fill-parent-horizontally! rect-margin!
   color color? color-r color-g color-b color-a color-r-set! color-g-set! color-b-set! color-a-set!
   widget? widget-type
   widget-gui widget-driver
   widget-parent widget-children
   reparent-widget! bring-widget-to-front! widget-focus-set! widget-clip-children?-set! destroy-widget!
   hide-widget! show-widget! widget-hidden?
   block-widget-input-events! unblock-widget-input-events! widget-input-events-blocked?
   enable-widget-debug! disable-widget-debug! debug-widget-enabled?
   enable-widget-hint! disable-widget-hint! query-widget-hint
   widget-tileset-surface widget-tileset-surface-set!
   widget-geometry widget-absolute-geometry widget-composed-geometry widget-geometry-set!
   widget-center-in-parent! widget-center-in-parent-vertically! widget-center-in-parent-horizontally! widget-layout-vertically! widget-layout-horizontally! widget-fill-parent-vertically! widget-fill-parent-horizontally! widget-margin!
   cursor-over-widget? cursor-pressed-on-widget? cursor-released-on-widget?
   frame frame?
   scrollbox scrollbox? scrollbox-horizontal-scroll! scrollbox-vertical-scroll!
   label label? label-text-set! label-icon-set! label-alignment-set! label-style-set! label-font label-font-set! label-text-color label-text-color-set! label-text-color-set?
   button button* button? button-label button-label-set!
   editbox editbox? editbox-text editbox-text-set! editbox-cursor-position editbox-cursor-position-set! editbox-font editbox-font-set! editbox-text-color editbox-text-color-set! editbox-text-color-set?
   checkbox checkbox? checkbox-label checkbox-label-set!
   toggle toggle? toggle-checked? toggle-checked?-set!
   handler-set! handler-remove!
   widgets widget-by-id)

  (import scheme)

  (cond-expand
   (chicken-4
    (import chicken foreign)
    (use extras data-structures lolevel
         srfi-1 srfi-4 srfi-69
         clojurian-syntax matchable))
   (chicken-5
    (import (chicken base))
    (import (chicken blob))
    (import (chicken condition))
    (import (chicken foreign))
    (import (chicken format))
    (import (chicken gc))
    (import (chicken locative))
    (import (chicken memory))
    (import (srfi 1))
    (import (srfi 4))
    (import (srfi 69))
    (import (clojurian syntax))
    (import matchable)))

  (include "kiwi.scm"))
