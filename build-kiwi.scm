(import scheme)
(import (chicken base))
(import (chicken file))
(import (chicken format))
(import (chicken io))
(import (chicken process))
(import (chicken process-context))
(import (chicken string))

(define (library-flags var command)
  (or (get-environment-variable var)
      (let ((exit (system (string-append command " > /dev/null"))))
        (if (zero? exit)
            (let ((output (with-input-from-pipe command (cut read-string #f))))
              (when (eof-object? output)
                (error (format "Command didn't produce any output: ~a" command)))
              (string-chomp output))
            (error (format "Command failed with exit code ~s, set $~a" exit var))))))

(define csc (get-environment-variable "CHICKEN_CSC"))
(define sdl2-cflags (library-flags "SDL2_CFLAGS" "sdl2-config --cflags"))
(define sdl2-ldlibs (library-flags "SDL2_LDLIBS" "sdl2-config --libs"))
(define kiwi-cflags (library-flags "KIWI_CFLAGS" "cmake --find-package -DNAME=KiWi -DCOMPILER_ID=GNU -DLANGUAGE=C -DMODE=COMPILE"))
(define kiwi-ldlibs (or (get-environment-variable "KIWI_LDLIBS") "-lKiWi"))

;; querying cmake leaves those behind
(when (directory-exists? "CMakeFiles")
  (delete-directory "CMakeFiles" #t))

(define args (list csc sdl2-cflags sdl2-ldlibs kiwi-cflags kiwi-ldlibs))
(define cmdline
  (string-append (apply format "~a -C ~a -L ~a -C ~a -L ~a " (map qs args))
                 (string-intersperse (map qs (command-line-arguments)) " ")))

(print cmdline)
(system* cmdline)
